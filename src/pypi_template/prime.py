import math

def isPrime(x):
    if x < 2:
        return False
    for n in range(2, math.isqrt(x) + 1):
        if x % n == 0:
            return False
    return True
