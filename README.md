# pypi-template

This project contains a template to create a pip package, version it automatically and publish it into the Gitlab package registry.

## jobs

### build_package

Using the `python:3.12.4-alpine3.20` image, builds the example package. Stores the package in artifacts.

### test_package

Installs the package from artifacts then using `unittest`, `coverage` and `unittest-xml-report` packages, executes the test cases and provides coverage and test result reports.

### deploy_package

Uploads the package from the build artifact using `twine` into the Gitlab package registry of the project.
