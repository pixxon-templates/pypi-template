import unittest
from parameterized import parameterized

from pypi_template import prime

class TestPrime(unittest.TestCase):

    @parameterized.expand([-1, 0, 1])
    def test_isPrime_ValuesLessThanTwo(self, x):
        self.assertFalse(prime.isPrime(x), f'{x} should not be prime')

    @parameterized.expand([2, 3, 5, 7])
    def test_isPrime_PrimesLessThanTen(self, x):
        self.assertTrue(prime.isPrime(x), f'{x} should be prime')
    
    @parameterized.expand([4, 6, 8, 9])
    def test_is_Prime_NonPrimesLessThanTen(self, x):
        self.assertFalse(prime.isPrime(x), f'{x} should not be prime')

if __name__ == '__main__':
    unittest.main()
